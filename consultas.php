<?php
require_once('config.php');
include('consultasController.php');

$data = mysqli_query($conn, sprintf("SELECT * FROM listar_consultas")) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($data);
$result = mysqli_num_rows($data);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Trabalho de Banco de Dados</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
<div class="topnav">
    <a href="index.php">Home</a>
    <a href="clientes.php">Clientes</a>
    <a href="medicos.php">Médicos</a>
    <a href="funcionarios.php">Funcionários</a>
    <a href="pagamentos.php">Pagamentos</a>
    <a href="planos.php">Planos</a>
    <a class="active" href="consultas.php">Consultas</a>
    <a href="exames.php">Exames</a>
</div>
<div>
    <?php
    // se o número de resultados for maior que zero, mostra os dados
    if($result > 0) {
        // inicia o loop que vai mostrar todos os dados ?>
        <table class="tabelas">
        <tr>
            <th>Nome da Consulta</th>
            <th>Nome do Médico</th>
            <th>Nome do Cliente</th>
            <th>Data da Consulta</th>
            <th>Valor Pago</th>
            <th>Editar</th>
            <th>Excluir</th>
        </tr>

        <?
        do {
            ?>
            <tr>
                <td><?=$row['nome_consulta'];?></td>
                <td><?=$row['nome_medico'];?></td>
                <td><?=$row['nome'];?></td>
                <td><?=date("d/m/Y", strtotime($row['dt_recebimento']));?></td>
                <td>R$: <?=number_format($row['recebimento_medico']+$row['comissao_clinica'], 2, ',','.');?></td>
                <td>
                    <center>
                        <form action="form_consultas.php" method="get" target="_top">
                            <input type="hidden" id="editar" name="editar">
                            <input type="hidden" id="id" name="id" value="<?=$row['id_consultas'];?>">
                            <button type="submit" class="btn-danger" id="editar">E</button>
                        </form>
                    </center>
                </td>
                <td>
                    <center>
                        <form action="consultas.php" method="get" target="_self">
                            <input type="hidden" id="deletar" name="deletar">
                            <input type="hidden" id="id" name="id" value="<?=$row['id_consultas'];?>">
                            <button type="submit" class="btn-danger" id="deletar">X</button>
                        </form>
                    </center>
                </td>
            </tr>
            <?
        }while($row = mysqli_fetch_assoc($data));

        ?></table><?
    }
    ?>
    <div class="col-lg-4">
        <a href="form_consultas.php" target="_top">
            <button type="button" class="botao btn-primary btn-xl">Cadastrar Consulta</button>
        </a>
    </div>


</div>
</body>
</html>
<?php
//check if the get variable exists
if (isset($_GET['deletar']))
{
    deletar($_GET['id'], $conn);
}

function deletar($id, $conn)
{
    $query = sprintf('CALL excluir_consulta('.$id.')');
    mysqli_query($conn, $query) or die(mysqli_error($conn));
    header('Location: http://bancodedados.freevar.com/consultas.php');
}

mysqli_free_result($data);
?>



