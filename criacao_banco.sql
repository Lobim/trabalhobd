CREATE TABLE cliente (
	id_cliente int(4) not null AUTO_INCREMENT,
    nome VARCHAR(100) not null,
    estado_civil varchar(15) not null,
	idade int(3) not null,
	doenca_cronica boolean,
    sexo varchar(10) not null,
    doenca_pre_existente varchar(100), 
    fk_id_exame int(4),
    fk_id_plano int(4),
	fk_id_endereco int(4),
	data_ultima_consulta date,
    PRIMARY KEY (id_cliente),
    UNIQUE (id_cliente)
)ENGINE=INNODB;

CREATE TABLE plano_saude (
    id_plano int(4) not null AUTO_INCREMENT,
    nome varchar(100) not null,
    registro_anvisa varchar(10) not null,
    valor_consulta float not null,
    PRIMARY KEY (id_plano),
    UNIQUE (id_plano)
)ENGINE=INNODB;

CREATE TABLE medico (
    crm int(10) not null,
	nome_medico varchar(100) not null,
    especialidade varchar(100) not null,
    escola_origem varchar(100) not null,
    tipo_resid_medica varchar(100) not null,
    estado_civil_medico varchar(10) not null,
    regime_trabalho char(4) not null,
    dt_nascimento date not null,
    fk_id_plano int(4),
    PRIMARY KEY (crm),
    UNIQUE (crm)
)ENGINE=INNODB;

CREATE TABLE `endereco` (
  `id_endereco` int(11) not null AUTO_INCREMENT,
  `rua` varchar(100) NOT NULL,
  `numero` int(11) NOT NULL,
  `complemento` varchar(100),
  `bairro` varchar(30) NOT NULL,
  `cidade` varchar(30) NOT NULL,
  `estado` varchar(30) NOT NULL,
  PRIMARY KEY (id_endereco),
  UNIQUE (id_endereco)
  
)ENGINE=INNODB;

CREATE TABLE funcionario_clinica (
    id_funcionario int(4) not null AUTO_INCREMENT,
    nome varchar(100) not null,
    dt_nascimento date not null,
    sexo varchar(10) not null,
    funcao varchar(100) not null,
    dt_admissao date not null,
    salario_bruto float not null,
    estado_civil varchar(10) not null,
    dependentes int(2),
    fk_endereco_id int(4) not null,
    fk_id_plano int(4),
    PRIMARY KEY (id_funcionario),
    UNIQUE (id_funcionario)
)ENGINE=INNODB;

CREATE TABLE info_pagamento (
	id_pagamento int(4) not null AUTO_INCREMENT,
    recebimento_medico float not null,
    comissao_clinica float,
    dt_recebimento date not null,
    dt_repasse_medico date not null,
    imposto_retido float not null,
    fk_crm int(10) not null,
    PRIMARY KEY (id_pagamento),
    UNIQUE (id_pagamento)
)ENGINE=INNODB;

CREATE TABLE consultas (
    id_consulta int(4) not null AUTO_INCREMENT,
    nome_consulta varchar(100) not null,
	fk_crm int not null,
	fk_id_exame int,
	fk_id_pagamento int,
	fk_id_cliente int not null,
    PRIMARY KEY (id_consulta),
    UNIQUE (id_consulta)
)ENGINE=INNODB;

CREATE TABLE exames (
    id_exame int(4) not null AUTO_INCREMENT,
    nome_exame varchar(100) not null,
	fk_crm int not null,
	fk_id_cliente int not null,
    PRIMARY KEY (id_exame),
    UNIQUE (id_exame)
)ENGINE=INNODB;

create table if not exists paciente_cronico(
	id_paciente_cronico int not null,
	data_consulta date not null,
	fk_id_cliente int not null,
	fk_id_consultas int not null,
	primary key (id_paciente_cronico),
	unique (id_paciente_cronico)
)ENGINE=INNODB;
 
ALTER TABLE cliente ADD CONSTRAINT FK_Cliente_2
    FOREIGN KEY (fk_id_plano)
    REFERENCES plano_saude (fk_id_plano)
    ON DELETE CASCADE
    ON UPDATE CASCADE;
 
ALTER TABLE cliente ADD CONSTRAINT FK_Cliente_4
    FOREIGN KEY (fk_id_exame)
    REFERENCES exames (fk_id_exame)
    ON DELETE CASCADE
    ON UPDATE CASCADE;
 
ALTER TABLE medico ADD CONSTRAINT FK_medico_2
    FOREIGN KEY (fk_id_plano)
    REFERENCES plano_saude (fk_id_plano)
    ON DELETE CASCADE
    ON UPDATE CASCADE;
 
ALTER TABLE funcionario_clinica ADD CONSTRAINT FK_funcionario_clinica_2
    FOREIGN KEY (fk_id_plano)
    REFERENCES plano_saude (fk_id_plano)
    ON DELETE CASCADE
    ON UPDATE CASCADE;
 
ALTER TABLE info_pagamento ADD CONSTRAINT FK_info_pagamento_2
    FOREIGN KEY (medico_crm)
    REFERENCES medico (crm)
    ON DELETE CASCADE
    ON UPDATE CASCADE;


#inclui clientes na tabela cliente#
DELIMITER $$
create procedure incluir_cliente (v_nome varchar(100), v_estado_civil varchar(15), v_sexo char(1), v_doenca varchar(100), v_idade int, v_fk_id_endereco int, v_doenca_cronica boolean)
begin
	insert into cliente (Nome, estado_civil , sexo, doenca_pre_existente, idade, fk_id_endereco, fk_id_plano, doenca_cronica)
    values (v_nome, v_estado_civil, v_sexo, v_doenca, v_idade, v_fk_id_endereco, v_fk_id_plano, v_doenca_cronica);
end;
$$

#Excluir cliente da tabela cliente#
delimiter $$
create procedure excluir_cliente (v_id_cliente int)
begin
	delete from cliente where Id_cliente = v_id_cliente;
end;
$$

#retorna o numero de clientes cadastrados
DELIMITER $$
create function qtd_cliente() returns int
begin
	return (select count(*) from cliente);
end;
$$

#listar clientes na tabela cliente#
create view listar_clientes as
select * from cliente left join endereco on cliente.fk_id_endereco = endereco.id_endereco;

#Exclui o endereço do cliente#
DELIMITER $$
create trigger exclui_endCliente after delete
on cliente for each row
begin
	delete from endereco where id_endereco = old.fk_id_endereco;
end;
$$

#Inclui paciente crônico#
DELIMITER $$
create trigger inclui_paciente_cronico after update, insert
on cliente for each row
begin
	delete from endereco where id_endereco = old.fk_id_endereco;
end;
$$

#Editar o cliente#
delimiter $$
CREATE  PROCEDURE editar_cliente(v_id_cliente int, v_nome varchar(100), v_estado_civil varchar(15), v_sexo varchar(10), v_doenca varchar(100), v_idade int, v_fk_id_endereco int, v_fk_id_plano int)
BEGIN
    UPDATE cliente SET Nome = v_nome, estado_civil = v_estado_civil , sexo = v_sexo, doenca_pre_existente = v_doenca, idade = v_idade, fk_id_endereco =  v_fk_id_endereco, fk_id_plano = v_fk_id_plano WHERE id_cliente = v_id_cliente;
END;
$$

#Tabela para verificação de doenças cronicas#
DELIMITER $$
create trigger doenca_cronica after insert
on consultas
for each row
begin
	if ((select cliente.doenca_cronica as doenca from consultas inner join cliente on fk_id_cliente = id_cliente order by id_consultas desc limit 1) = 1) then
		insert into paciente_cronico (data_consulta, fk_id_cliente, fk_id_consultas)
		values(curdate(), new.fk_id_cliente, new.id_consultas);
	end if;
end
$$

#-------------------------------Tabela Endereço------------------------------------------#

#inclui endereço na tabela endereço#
DELIMITER $$
create procedure incluir_endereco (v_rua varchar(200), v_numero int, v_complemento varchar(200), v_bairro varchar(100), v_cidade varchar(200), v_estado varchar(100))
begin
	insert into endereco (rua, numero, complemento, bairro, cidade, estado)
    values (v_rua, v_numero, v_complemento, v_bairro, v_cidade, v_estado);
end;
$$

#Edita a tabela endereco#
delimiter $$
CREATE  PROCEDURE editar_endereco(v_id_endereco int, v_rua varchar(200), v_numero int, v_complemento varchar(200), v_bairro varchar(100), v_cidade varchar(200), v_estado varchar(100))
BEGIN
    UPDATE endereco SET rua = v_rua, numero = v_numero, complemento = v_complemento, bairro = v_bairro, cidade = v_cidade, estado = v_estado WHERE id_endereco = v_id_endereco;
END;
$$

#-------------------------------Tabela Exames----------------------------------------#

#inclui exames na tabela exames#
DELIMITER $$
create procedure incluir_exame (v_nome varchar(100), v_fk_crm int, v_fk_id_cliente int, v_valor_exame float)
begin
	insert into exames (nome_exame)
    values (v_nome);
end;
$$

#editar exames na tabela exames#
DELIMITER $$
create procedure editar_exame (v_nome varchar(100), v_id_exame int)
begin
	UPDATE exames SET nome_exame = v_nome WHERE id_exame = v_id_exame;
end;
$$

#Excluir exame da tabela exames#
DELIMITER $$
create procedure excluir_exame (v_id_exame int)
begin
	delete from exames where id_exame = v_id_exame;
end;
$$

#listar clientes na tabela cliente#
create view listar_exames as
select * from exames


#-------------------------------Tabela Funcionarios------------------------------------------#

#inclui funcionarios na tabela funcionarios#
DELIMITER $$
create procedure incluir_funcionario (v_nome varchar(100), v_dt_nascimento date, v_sexo char(1), v_endereco varchar(200), v_funcao varchar(100), v_dt_admissao date, v_salario_bruto float, v_estado_civil char(1), v_dependentes int(2), v_fk_id_plano int)
begin
	insert into funcionario_clinica (nome, dt_nascimento, fk_endereco_id, sexo, funcao, dt_admissao, salario_bruto, estado_civil, dependentes, fk_id_plano)
    values (v_nome, v_dt_nascimento, v_endereco, v_sexo, v_funcao, v_dt_admissao, v_salario_bruto, v_estado_civil, v_dependentes, v_fk_id_plano);
end;
$$

#excluir funcionarios na tabela funcionarios#
DELIMITER $$
create procedure excluir_funcionario (v_id_funcionario int(4))
begin
	delete from exames where id_funcionario = v_id_funcionario;
end;
$$

#Exclui o endereço do funcionario#
DELIMITER $$
create trigger exclui_endFuncionario after delete
on funcionario_clinica for each row
begin
	delete from endereco where id_endereco = old.fk_endereco_id;
end$$

#listar funcionarios na tabela de funcionarios#
create view listar_funcionario as
select * from funcionario_clinica inner join endereco on fk_endereco_id = id_endereco;

#editar funcionarios na tabela funcionarios#
DELIMITER $$
create procedure editar_funcionario (v_id_funcionario int, v_nome varchar(100), v_dt_nascimento date, v_sexo varchar(10), v_fk_id_endereco varchar(200), v_funcao varchar(100), v_dt_admissao date, v_salario_bruto float, v_estado_civil char(1), v_dependentes int(2), v_fk_id_plano int)
begin
	UPDATE funcionario_clinica SET nome = v_nome, dt_nascimento = v_dt_nascimento, fk_id_endereco = v_fk_id_endereco, sexo = v_sexo, funcao = v_funcao, dt_admissao = v_dt_admissao, salario_bruto = v_salario_bruto, estado_civil = v_estado_civil, dependentes = v_dependentes, fk_id_plano = v_fk_id_plano WHERE id_funcionario = v_id_funcionario;
end;
$$

#-------------------------------Tabela pagamentos------------------------------------------#

#inclui pagamentos na tabela info_pagamaneto#
DELIMITER $$
create procedure incluir_pagamento (v_recebimento_medico float, v_comissao_clinica float, v_dt_recebimento date, v_dt_repasse_medico date, v_fk_crm int)
begin
	declare v_imposto_retido float;
	set v_imposto_retido = (v_recebimento_medico) * (5 / 100);
	insert into info_pagamento (recebimento_medico, comissao_clinica, dt_recebimento, dt_repasse_medico, imposto_retido, fk_crm)
    values (v_recebimento_medico, v_comissao_clinica, v_dt_recebimento, v_dt_repasse_medico, v_imposto_retido, v_fk_crm);
end
end;
$$

#excluir pagamento na tabela pagamento#
DELIMITER $$
create procedure excluir_pagamento (v_id_pagamento int)
begin
	delete from info_pagamento where id_pagamento = v_id_pagamento;
end;
$$

#-------------------------------Tabela medico------------------------------------------#

#inclui medico na tabela medico#
DELIMITER $$
create procedure incluir_medico (v_crm int, v_nome_medico varchar(100), v_especialidade varchar(100), v_escola_origem varchar(200), v_tipo_resid_medica varchar(100), v_estado_civil varchar(10), v_regime_trabalho char(10), v_plano_atende int, v_dt_nascimento date)
begin
	insert into info_pagamento (crm, nome_medico, especialidade, escola_origem, tipo_resid_medica, estado_civil, regime_trabalho, plano_atende, dt_nascimento)
    values (v_crm, v_nome_medico, v_especialidade, v_escola_origem, v_tipo_resid_medica, v_estado_civil, v_regime_trabalho, v_plano_atende, v_dt_nascimento);
end;
$$

#excluir medico na tabela medico#
DELIMITER $$
create procedure excluir_medico (v_crm varchar(100))
begin
	delete from medico where crm = v_crm;
end;
$$

#editar medico na tabela medico#
DELIMITER $$
create procedure editar_medico (v_crm int, v_nome_medico varchar(100), v_especialidade varchar(100), v_escola_origem varchar(200), v_tipo_resid_medica varchar(100), v_estado_civil varchar(10), v_regime_trabalho char(10), v_fk_id_plano int, v_dt_nascimento date)
begin
	UPDATE medico SET nome_medico = v_nome_medico, especialidade = v_especialidade, escola_origem = v_escola_origem, tipo_resid_medica = v_tipo_resid_medica, estado_civil = v_estado_civil, regime_trabalho = v_regime_trabalho, fk_id_plano = v_fk_id_plano, dt_nascimento = v_dt_nascimento WHERE crm = v_crm;
end;
$$

#listar medicos da tabela medicos#
create view listar_medico as
select medico.*, plano_saude.nome from medico inner join plano_saude on fk_id_plano = id_plano;

#-------------------------------Tabela planos de saude------------------------------------------#

#inclui plano de saude na tabela plano_saude#
DELIMITER $$
create procedure incluir_plano (v_nome varchar(100), v_registro_anvisa varchar(100), v_valor_consulta float)
begin
	insert into plano_saude (nome, registro_anvisa, valor_consulta)
    values (v_nome, v_registro_anvisa, v_valor_consulta);
end;
$$

#excluir plano na tabela plano_saude#
DELIMITER $$
create procedure excluir_plano (v_id_plano varchar(100))
begin
	delete from plano_saude where id_plano = v_id_plano;
end;
$$

#-------------------------------Tabela de consultas------------------------------------------#

#inclui consulta na tabela de consultas#
DELIMITER $$
create procedure incluir_consulta (v_nome_consulta varchar(100), v_fk_id_cliente int, v_fk_id_exame int, v_fk_crm int, v_fk_id_pagamento int)
begin
	insert into consulta (nome_consulta, fk_id_cliente, fk_id_exame, fk_crm, fk_id_pagamento)
    values (v_nome_consulta, v_fk_id_cliente, v_fk_id_exame, v_fk_crm, v_fk_id_pagamento);
end;
$$

#editar consulta na tabela de consultas#
DELIMITER $$
create procedure editar_consulta (v_id_consultas int, v_nome_consulta varchar(100), v_fk_id_cliente int, v_fk_id_exame int, v_fk_crm int, v_fk_id_pagamento int)
begin
	 UPDATE consultas SET nome_consulta = v_nome_consulta, fk_id_cliente = v_fk_id_cliente, fk_id_exame = v_fk_id_exame, fk_id_pagamento = v_fk_id_pagamento, fk_crm = v_fk_crm WHERE id_consultas = v_id_consultas;
end;
$$


#Excluir consulta da tabela consultas#
DELIMITER $$
create procedure excluir_consulta (v_id_consulta int)
begin
	delete from consultas where id_consultas = v_id_consulta;
end;
$$


#listar consultas na tabela consultas#
create view listar_consultas as
select consultas.id_consultas, consultas.fk_id_exame, consultas.fk_crm , consultas.fk_id_cliente, consultas.fk_id_pagamento, consultas.nome_consulta , exames.nome_exame, exames.id_exame, medico.crm, medico.nome_medico, cliente.id_cliente, cliente.nome, info_pagamento.id_pagamento, info_pagamento.recebimento_medico, info_pagamento.comissao_clinica  from consultas 
						left outer join exames on consultas.fk_id_exame = exames.id_exame
					    left outer join medico on consultas.fk_crm = medico.crm
				        left outer join cliente on consultas.fk_id_cliente = cliente.id_cliente
						left outer join info_pagamento on consultas.fk_id_pagamento = info_pagamento.id_pagamento;


#------------------------------------------Perguntas a serem respondidas------------------------------------------#

#1) Quantas consultas foram realizadas num determinado período e os valores recebidos #
DELIMITER $$
create procedure pagamentos_periodo (v_dt_recebimento_inicial date, v_dt_recebimento_final date)
begin
	select 
		(select count(dt_recebimento) from info_pagamento where dt_recebimento between v_dt_recebimento_inicial AND v_dt_recebimento_final) as consultas_periodo , 
		(select  sum(recebimento_medico) from info_pagamento where dt_recebimento between v_dt_recebimento_inicial AND v_dt_recebimento_final) as valor_consultas;
end;
$$

#2) Qual(is) médicos atenderam quais pacientes;#
create view atendimentos_medicos as
SELECT cliente.nome as cliente, medico.nome_medico as medico FROM consultas 
inner join medico on fk_crm = crm
inner join cliente on fk_id_cliente = id_cliente order by medico

#3) Qual o valor pago a cada médico, menos a comissão;#
create view pagamento_medicos as
select medico.nome_medico as Medico, (recebimento_medico - comissao_clinica) as valor_recebido from medico inner join info_pagamento on medico.crm = fk_crm order by medico.nome_medico, valor_recebido;

#4) Qual o valor arrecadado com comissões#
create view valor_comissoes as
select sum(comissao_clinica) as valor_total from info_pagamento;

#5) Qual médico atende mais pacientes#
create view medico_maisAtende as
SELECT cliente.nome as cliente, medico.nome as medico, (select count(distinct fk_crm) from consultas) as quantidade FROM consultas 
inner join medico on fk_crm = crm
inner join cliente on fk_id_cliente = id_cliente group by medico order by count(*) desc limit 1;

#6) Qual especialidade atende mais e arrecada mais; #
create view specialidades_maisAtende as
SELECT medico.especialidade as espec_maisAtende, (select count(distinct fk_crm) from consultas) as quantidade FROM consultas 
inner join medico on fk_crm = crm
group by espec_maisAtende order by count(*) desc limit 1;

create view specialidades_maisArrecada as
SELECT medico.especialidade as espec_maisArrecada, (select sum(distinct recebimento_medico) from info_pagamento) as valor FROM consultas 
inner join info_pagamento on id_pagamento = fk_id_pagamento
inner join medico on crm = consultas.fk_crm
group by medico.especialidade
order by count(*) desc 
limit 1;




#7) O paciente em tratamento crônico não pode passar mais de 6 meses entre consultas, o sistema deverá informar automaticamente; #
create view seis_meses_cronicos as
SELECT DATEDIFF(CURDATE(),cliente.data_ultima_consulta) as datas, nome FROM cliente WHERE doenca_cronica = 1





#8) Informação planos de saúde: Quais planos são mais atendidos;#
create view plano_maisAtende as
select plano_saude.nome as plano, count(*) as qtd from cliente inner join plano_saude on fk_id_plano = id_plano group by plano order by qtd desc limit 1;

#9) Qual a média e a mediana da idade dos pacientes;#

#Média de idade dos pacientes#
DELIMITER $$
create function media_idade() returns float
begin
	declare media float;
	set media =  (select avg(idade) from cliente);
	return media;
end;
$$


#Mediana de idade dos pacientes#
DELIMITER $$
create function mediana_idade() returns float
begin
begin
	return(
	SELECT AVG(mid_vals) AS 'mediana' FROM (
 SELECT idade AS 'mid_vals' FROM
  (
   SELECT @row:=@row+1 AS 'row', a.idade
   FROM cliente AS a, (SELECT @row:=0) AS r
   ORDER BY a.idade
  ) AS tab1,
  (
   SELECT COUNT(*) as 'count'
   FROM cliente x
  ) AS tab2
  WHERE tab1.row >= tab2.count/2 and tab1.row <= ((tab2.count/2) +1)) AS tab3);
  
end;
$$

#10) Listar os médicos que são prestadores de serviço; #
create view prestador_servico as
select nome_medico from medico where regime_trabalho = "pj";
