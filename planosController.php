<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $plano = new stdClass();
    $plano->nome = $_POST['nome'];
    $plano->registro_anvisa = $_POST['registro_anvisa'];
    $plano->valor_consulta = $_POST['valor_consulta'];

    if ($_POST['id_plano']) {
        $plano->id_plano = $_POST['id_plano'];
        $query = sprintf('CALL editar_plano("' . $plano->id_plano . '","' . $plano->nome . '","' . $plano->registro_anvisa . '","' . $plano->valor_consulta . '")');
    } else {
        $query = sprintf('CALL incluir_plano("' . $plano->nome . '","' . $plano->registro_anvisa . '","' . $plano->valor_consulta . '")');
    }
    mysqli_query($conn, $query) or die(mysqli_error($conn));
    header('http://bancodedados.freevar.com/planos.php');
}
