<?php
require_once('config.php');

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // collect value of input field
    $info_pagamento = new stdClass();
    $info_pagamento->recebimento_medico = $_POST['recebimento_medico'];
    $info_pagamento->comissao_clinica = $_POST['comissao_clinica'];
    $info_pagamento->dt_recebimento = $_POST['dt_recebimento'];
    $info_pagamento->dt_repasse_medico = $_POST['dt_repasse_medico'];
    $info_pagamento->imposto_retido = $_POST['imposto_retido'];
    $info_pagamento->fk_crm = $_POST['medico'];

    if ($_POST['id_pagamento']) {
        $info_pagamento->id_pagamento = $_POST['id_pagamento'];
        $query = sprintf('CALL editar_pagamento("' . $info_pagamento->id_pagamento . '","' . $info_pagamento->recebimento_medico . '","' . $info_pagamento->comissao_clinica . '","' . $info_pagamento->dt_recebimento . '","' . $info_pagamento->dt_repasse_medico . '","' . $info_pagamento->fk_crm . '")');
    } else {
        $query = sprintf('CALL incluir_pagamento("' . $info_pagamento->recebimento_medico . '","' . $info_pagamento->comissao_clinica . '","' . $info_pagamento->dt_recebimento . '","' . $info_pagamento->dt_repasse_medico . '","' . $info_pagamento->fk_crm . '")');
    }
    mysqli_query($conn, $query) or die(mysqli_error($conn));
    header('http://bancodedados.freevar.com/pagamentos.php');
}

