<?php
require_once('config.php');
include('indexController.php');

function date_interval($valor)
{
    $meses = $valor / 30;
    switch ($meses) {
        case $meses <= 4:
            return "Menos de 4 meses";
        case $meses < 6:
            return "Atenção, quase 6 meses";
        case $meses >= 6:
            return "Reagendar Urgente";

    }
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Trabalho de Banco de Dados</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>
<body>
<div class="topnav">
    <a class="active" href="index.php">Home</a>
    <a href="clientes.php">Clientes</a>
    <a href="medicos.php">Médicos</a>
    <a href="funcionarios.php">Funcionários</a>
    <a href="pagamentos.php">Pagamentos</a>
    <a href="planos.php">Planos</a>
    <a href="consultas.php">Consultas</a>
    <a href="exames.php">Exames</a>
</div>
<div>
    <table>
        <tr>
            <b>Perguntas essenciais a serem respondidas: </b><br>
        </tr>
        <tr>
            <b>1) Quantas consultas foram realizadas num determinado período e os valores recebidos</b> <br><br>
            <form action="index.php" method="post" class="formulario">
                <table>
                    <tr>
                        <td>
                            <input type="hidden" id="questao1" name="questao1" value="questao1">
                            Data Inicial: <input type="date" id="data_inicial" name="data_inicial"/></p>
                        </td>
                        <td>
                            Data Final: <input type="date" id="data_final" name="data_final"/></p>
                        </td>
                    </tr>
                </table>
                <p><input type="submit" value="Resposta"/></p>
            </form>
        </tr>
        <tr>
            <b>Resultado:</b> <br>
            <b> Quantidade de Consultas:</b> <?php echo $resposta_questao1['consultas_periodo'] ?> <br>
            <b>Total Valores Recebidos:</b> <?php echo $resposta_questao1['valor_consultas'] ?> <br>
        </tr>
        <tr><br></tr>
        <tr>
            <b>2) Qual(is) médicos atenderam quais pacientes;</b> <br>
        </tr>
        <tr>
            <?php
            // se o número de resultados for maior que zero, mostra os dados
            if ($result_atendimentos > 0) {
                // inicia o loop que vai mostrar todos os dados ?>
                <table class="tabelas">
                <tr>
                    <th>Nome do Cliente</th>
                    <th>Nome do Médico</th>
                </tr>

                <?
                do {
                    ?>
                    <tr>
                        <td><?= $row_atendimentos['cliente']; ?></td>
                        <td><?= $row_atendimentos['medico']; ?></td>
                    </tr>
                    <?
                } while ($row_atendimentos = mysqli_fetch_assoc($data_atendimentos));

                ?></table><?
            }
            ?>
        </tr>
        <tr>
            <form action="index.php" method="post" class="formulario">
                <table>
                    <input type="hidden" id="questao2" name="questao2" value="questao2">
                </table>
                <p><input type="submit" value="Resposta"/></p>
            </form>
        </tr>
        <tr>
            <b>3) Qual o valor pago a cada médico, menos a comissão; </b><br>
        </tr>
        <tr>
            <?php
            // se o número de resultados for maior que zero, mostra os dados
            if ($result_pagamentos_medicos > 0) {
                // inicia o loop que vai mostrar todos os dados ?>
                <table class="tabelas">
                <tr>
                    <th>Nome do Médico</th>
                    <th>Valor Recebido:</th>
                </tr>

                <?
                do {
                    ?>
                    <tr>
                        <td><?= $row_pagamentos_medicos['Medico']; ?></td>
                        <td>R$: <?= number_format($row_pagamentos_medicos['valor_recebido'], 2, ',', '.'); ?></td>
                    </tr>
                    <?
                } while ($row_pagamentos_medicos = mysqli_fetch_assoc($data_pagamentos_medicos));

                ?></table><?
            }
            ?>
        </tr>
        <tr>
            <form action="index.php" method="post" class="formulario">
                <table>
                    <input type="hidden" id="questao3" name="questao3" value="questao3">
                </table>
                <p><input type="submit" value="Resposta"/></p>
            </form>
        </tr>
        <tr>
            <b>4) Qual o valor arrecadado com comissões </b><br>
        </tr>
        <tr>
            <b>Valor total:</b> R$: <?= number_format($data_total_comissoes['valor_total'], 2, ',', '.'); ?> <br>
        </tr>
        <tr>
            <form action="index.php" method="post" class="formulario">
                <table>
                    <input type="hidden" id="questao4" name="questao4" value="questao4">
                </table>
                <p><input type="submit" value="Resposta"/></p>
            </form>
        </tr>
        <tr>
            <b>5) Qual médico atende mais pacientes; </b><br>
        </tr>
        <tr>
            <b>Médico que mais atendeu pacientes:</b> <?= $data_medico_mais_atende['medico'] ?> <br>
            <b>Quantidade de atendimentos:</b> <?= $data_medico_mais_atende['quantidade'] ?> <br>
        </tr>
        <tr>
            <form action="index.php" method="post" class="formulario">
                <table>
                    <input type="hidden" id="questao5" name="questao5" value="questao5">
                </table>
                <p><input type="submit" value="Resposta"/></p>
            </form>
        </tr>
        <tr>
            <b> 6) Qual especialidade atende mais e arrecada mais; </b><br>
        </tr>
        <tr>
            <b>Especialidade que mais atende:</b> <?= $especialidade['atende']['espec_maisAtende'] ?><br>
            <b>Número de atendimentos:</b> <?= $especialidade['atende']['quantidade'] ?> <br>
            <b>Especialidade que mais arrecada:</b> <?= $especialidade['arrecada']['espec_maisArrecada'] ?><br>
            <b>Valor Arrecadado:</b> R$: <?= number_format($especialidade['arrecada']['valor'], 2, ',', '.'); ?> <br>
        </tr>
        <tr>
            <form action="index.php" method="post" class="formulario">
                <table>
                    <input type="hidden" id="questao6" name="questao6" value="questao6">
                </table>
                <p><input type="submit" value="Resposta"/></p>
            </form>
        </tr>
        <tr>
            <b>7) O paciente em tratamento crônico não pode passar mais de 6 meses entre consultas, o sistema deverá
                informar automaticamente;</b> <br>
        </tr>
        <tr>
            <?php
            // se o número de resultados for maior que zero, mostra os dados
            if ($result_cronicos > 0) {
                // inicia o loop que vai mostrar todos os dados ?>
                <table class="tabelas">
                <tr>
                    <th>Nome do Paciente</th>
                    <th>Data:</th>
                </tr>

                <?
                do {
                    ?>
                    <tr>
                        <td><?= $row_cronicos['nome']; ?></td>
                        <td><?php echo date_interval($row_cronicos['datas']); ?></td>

                    </tr>
                    <?
                } while ($row_cronicos = mysqli_fetch_assoc($data_cronicos));

                ?></table><?
            }
            ?>
        </tr>
        <tr>
            <form action="index.php" method="post" class="formulario">
                <table>
                    <input type="hidden" id="questao7" name="questao7" value="questao7">
                </table>
                <p><input type="submit" value="Resposta"/></p>
            </form>
        </tr>
        <tr>
            <b> 8) Informação planos de saúde: Quais planos são mais atendidos; </b><br>
        </tr>
        <tr>
            <b>Plano que mais atende:</b> <?= $data_plano_mais_atende['plano'] ?><br>
            <b>Quantidade:</b> <?= $data_plano_mais_atende['qtd'] ?><br>
        </tr>
        <tr>
            <form action="index.php" method="post" class="formulario">
                <table>
                    <input type="hidden" id="questao8" name="questao8" value="questao8">
                </table>
                <p><input type="submit" value="Resposta"/></p>
            </form>
        </tr>
        <tr>
            <b> 9) Qual a média e a mediana da idade dos pacientes;</b> <br>
        </tr>
        <tr>
            <b>Média de idade:</b> <?= $data_media['media'] ?><br>
            <b>Mediana de Idade:</b> <?= $data_mediana['mediana'] ?><br>
        </tr>
        <tr>
            <form action="index.php" method="post" class="formulario">
                <table>
                    <input type="hidden" id="questao9" name="questao9" value="questao9">
                </table>
                <p><input type="submit" value="Resposta"/></p>
            </form>
        </tr>
        <tr>
            <b>
                10) Listar os médicos que são prestadores de serviço; <br>
            </b>
        </tr>
        <tr>
            <?php
            // se o número de resultados for maior que zero, mostra os dados
            if ($result_prestadores > 0) {
                // inicia o loop que vai mostrar todos os dados ?>
                <table class="tabelas">
                <tr>
                    <th>Nome do Médico</th>
                </tr>

                <?
                do {
                    ?>
                    <tr>
                        <td><?= $row_prestadores['nome_medico']; ?></td>
                    </tr>
                    <?
                } while ($row_prestadores = mysqli_fetch_assoc($data_prestadores));

                ?></table><?
            }
            ?>
        </tr>
        <tr>
            <form action="index.php" method="post" class="formulario">
                <table>
                    <input type="hidden" id="questao10" name="questao10" value="questao10">
                </table>
                <p><input type="submit" value="Resposta"/></p>
            </form>
        </tr>
    </table>
</div>
<?php

mysqli_close($conn);

?>
</body>
</html>



