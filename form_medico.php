<?php

require_once('config.php');
include('medicosController.php');

$query = sprintf("SELECT * FROM plano_saude");

$data = mysqli_query($conn, $query) or die(mysqli_error($conn));

while ($row = mysqli_fetch_assoc($data)) {
    $planos[] = $row;
}
$result = mysqli_num_rows($data);

if (isset($_GET['editar'])) {

    $query_medico = sprintf('SELECT * FROM medico WHERE crm=' . $_GET['crm'] . '');
    $medico_edita = mysqli_fetch_object(mysqli_query($conn, $query_medico)) or die(mysqli_error($conn));
}

function preencherValores($valor, $item)
{
    if (isset($valor) && $valor == $item) {
        return "selected";
    }
    return "";
}

?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Trabalho de Banco de Dados</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>

<body>
<div class="topnav">
    <a href="index.php">Home</a>
    <a href="clientes.php">Clientes</a>
    <a class="active" href="medicos.php">Médicos</a>
    <a href="funcionarios.php">Funcionários</a>
    <a href="pagamentos.php">Pagamentos</a>
    <a href="planos.php">Planos</a>
    <a href="consultas.php">Consultas</a>
    <a href="exames.php">Exames</a>
</div>

<div>
    <form action="medicos.php" method="post" class="formulario">

        <table>

            <tr>
                <?php if (!empty($medico_edita->crm) && mysqli_num_rows(mysqli_query($conn, sprintf('SELECT * FROM medico WHERE crm = ' . $medico_edita->crm))) > 0) { ?>
                    <p>crm: <input type="text" name="crm" placeholder="Crm do médico..."
                                   value="<?php echo $medico_edita->crm ?>" readonly/></p>
                <?php } else { ?>
                    <p>crm: <input type="text" name="crm" placeholder="Crm do médico..."
                                   value="<?php echo $medico_edita->crm ?>"/></p>
                <?php } ?>
                <p>Nome: <input type="text" name="nome_medico" placeholder="Nome do médico..."
                                value="<?php echo $medico_edita->nome_medico ?>"/></p>
                <p>Data de Nascimento: <input type="date" name="dt_nascimento"
                                              value="<?php echo $medico_edita->dt_nascimento ?>"/></p>
                <p>Estado Civil: <input type="text" name="estado_civil" placeholder="Estado Civil..."
                                        value="<?php echo $medico_edita->estado_civil_medico ?>"/></p>
                <p>Escola de Orígem: <input type="text" name="escola_origem" placeholder="Escola de Origem..."
                                            value="<?php echo $medico_edita->escola_origem ?>"/></p>
                <p>Tipo de Residencia Médica: <input type="text" name="tipo_resid_medica"
                                                     placeholder="Tipo de Residência Médica..."
                                                     value="<?php echo $medico_edita->tipo_resid_medica ?>"/></p>
                <p>Especialidade: <input type="text" name="especialidade" placeholder="Especialidade..."
                                         value="<?php echo $medico_edita->especialidade ?>"/></p>

                <label for="plano_saude">Plano que Atende:</label>

                <select name="plano_saude" id="plano_saude">

                    <?php
                    foreach ($planos as $plano_saude): ?>
                        <option <?php echo preencherValores($medico_edita->fk_id_plano, $plano_saude['id_plano']) ?>
                                value="<?php echo $plano_saude['id_plano'] ?>"><?php echo $plano_saude['nome'] ?></option>
                    <?php endforeach ?>
                </select>

                <p>Regime de Trabalho: <input type="text" name="regime_trabalho" placeholder="Regime de trabalho..."
                                              value="<?php echo $medico_edita->regime_trabalho ?>"/></p>
            </tr>

        </table>
        <p><input type="submit" value="Salvar"/></p>
    </form>
</div>

<div>
    <div class="col-lg-4">
        <a href="medicos.php" target="_self">
            <button type="button" class="botao btn-primary btn-xl">
                <div>
                    <h3>Voltar</h3>
                </div>
            </button>
        </a>
    </div>
</div>
</body>
</html>