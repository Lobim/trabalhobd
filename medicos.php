<?php
require_once('config.php');
include('medicosController.php');

$data = mysqli_query($conn, sprintf("SELECT * FROM medico")) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($data);
$result = mysqli_num_rows($data);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Trabalho de Banco de Dados</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
<div class="topnav">
    <a href="index.php">Home</a>
    <a href="clientes.php">Clientes</a>
    <a class="active" href="medicos.php">Médicos</a>
    <a href="funcionarios.php">Funcionários</a>
    <a href="pagamentos.php">Pagamentos</a>
    <a href="planos.php">Planos</a>
    <a href="consultas.php">Consultas</a>
    <a href="exames.php">Exames</a>
</div>
<div>
<?php
// se o número de resultados for maior que zero, mostra os dados
if($result > 0) {
    // inicia o loop que vai mostrar todos os dados ?>
    <table class="tabelas">
    <tr>
        <th>crm</th>
        <th>nome</th>
        <th>Data de Nascimento</th>
        <th>Estado Civil</th>
        <th>Escola de Origem</th>
        <th>Residência médica</th>
        <th>Especialidade</th>
        <th>Planos que atende</th>
        <th>Regime de trabalho</th>
        <th>Editar</th>
        <th>Deletar</th>
    </tr>
    <?
    do {
        ?>
        <tr>
            <td><?=$row['crm'];?></td>
            <td><?=$row['nome_medico'];?></td>
            <td><?=date("d/m/Y", strtotime($row['dt_nascimento']));?></td>
            <td><?=$row['estado_civil_medico'];?></td>
            <td><?=$row['escola_origem'];?></td>
            <td><?=$row['tipo_resid_medica'];?></td>
            <td><?=$row['especialidade'];?></td>
            <td><?= mysqli_fetch_object(mysqli_query($conn, sprintf("SELECT nome FROM listar_plano WHERE id_plano=".$row['fk_id_plano'])))->nome;?></td>
            <td><?=$row['regime_trabalho'];?></td>
            <td>
                <center>
                    <form action="form_medico.php" method="get" target="_top">
                        <input type="hidden" id="editar" name="editar">
                        <input type="hidden" id="crm" name="crm" value="<?=$row['crm'];?>">
                        <button type="submit" class="btn-danger" id="editar">E</button>
                    </form>
                </center>
            </td>
            <td>
                <center>
                    <form action="medicos.php" method="get" target="_self">
                        <input type="hidden" id="deletar" name="deletar">
                        <input type="hidden" id="crm" name="crm" value="<?=$row['crm'];?>">
                        <button type="submit" class="btn-danger" id="deletar">X</button>
                    </form>
                </center>
            </td>
        </tr>
    <?

    }while($row = mysqli_fetch_assoc($data));

    ?></table><?
}
?>
    <div class="col-lg-4">
        <a href="form_medico.php" target="_top">
            <button type="button" class="botao btn-primary btn-xl">Cadastrar Médico</button>
        </a>
    </div>
</div>
</body>
</html>

<?php
//check if the get variable exists
if (isset($_GET['deletar']))

{

    deletar($_GET['crm'], $conn);

}

function deletar($crm, $conn)

{

    $query = sprintf('CALL excluir_medico('.$crm.')');

    mysqli_query($conn, $query) or die(mysqli_error($conn));

    header('Location: http://bancodedados.freevar.com/medicos.php');

}
//check if the get variable exists
mysqli_free_result($data);
?>