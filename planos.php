<?php
require_once('config.php');
include('planosController.php');

$data = mysqli_query($conn, sprintf("SELECT * FROM listar_plano")) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($data);
$result = mysqli_num_rows($data);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Trabalho de Banco de Dados</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>
<body>
<div class="topnav">
    <a href="index.php">Home</a>
    <a href="clientes.php">Clientes</a>
    <a href="medicos.php">Médicos</a>
    <a href="funcionarios.php">Funcionários</a>
    <a href="pagamentos.php">Pagamentos</a>
    <a class="active" href="planos.php">Planos</a>
    <a href="consultas.php">Consultas</a>
    <a href="exames.php">Exames</a>
</div>
<div>
    <?php

    if ($result > 0) {
        ?>
        <table class="tabelas">
        <tr>
            <th>Nome</th>
            <th>Registro Anvisa</th>
            <th>Valor</th>
            <th>Editar</th>
            <th>Deletar</th>
        </tr>

        <?
        do {
            ?>
            <tr>
                <td><?= $row['nome']; ?></td>
                <td><?= $row['registro_anvisa']; ?></td>
                <td><?= $row['valor_consulta']; ?></td>
                <td>
                    <center>
                        <form action="form_planos.php" method="get" target="_top">
                            <input type="hidden" id="editar" name="editar">
                            <input type="hidden" id="id" name="id" value="<?= $row['id_plano']; ?>">
                            <button type="submit" class="btn-danger" id="editar">E</button>
                        </form>
                    </center>
                </td>
                <td>
                    <center>
                        <form action="planos.php" method="get" target="_self">
                            <input type="hidden" id="deletar" name="deletar">
                            <input type="hidden" id="id" name="id" value="<?= $row['id_plano']; ?>">
                            <button type="submit" class="btn-danger" id="deletar">X</button>
                        </form>
                    </center>
                </td>
            </tr>
            <?
        } while ($row = mysqli_fetch_assoc($data));

        ?></table><?
    }
    ?>
    <div class="col-lg-4">
        <a href="form_planos.php" target="_top">
            <button type="button" class="botao btn-primary btn-xl">Cadastrar Planos</button>
        </a>
    </div>


</div>
</body>
</html>
<?php
//check if the get variable exists
if (isset($_GET['deletar'])) {
    deletar($_GET['id'], $conn);
}

function deletar($id, $conn)
{
    $query = sprintf('CALL excluir_plano(' . $id . ')');
    mysqli_query($conn, $query) or die(mysqli_error($conn));
    header('Location: http://bancodedados.freevar.com/planos.php');
}

//check if the get variable exists

mysqli_free_result($data);
?>



