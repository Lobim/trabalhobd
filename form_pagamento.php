<?php
require_once('config.php');
include('pagamentosController.php');

$query = sprintf("SELECT * FROM medico order by nome_medico");
$data = mysqli_query($conn, $query) or die(mysqli_error($conn));

while ($row = mysqli_fetch_assoc($data)) {
    $medicos[] = $row;
}

if (isset($_GET['editar'])) {

    $query_pagamento = sprintf('SELECT * FROM info_pagamento WHERE id_pagamento=' . $_GET['id'] . '');
    $pagamento_edita = mysqli_fetch_object(mysqli_query($conn, $query_pagamento)) or die(mysqli_error($conn));

}

function preencherValores($valor, $item)
{
    if (isset($valor) && $valor == $item) {
        return "selected";
    }
    return "";
}

?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Trabalho de Banco de Dados</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>

<body>
<div class="topnav">
    <a href="index.php">Home</a>
    <a href="clientes.php">Clientes</a>
    <a href="medicos.php">Médicos</a>
    <a href="funcionarios.php">Funcionários</a>
    <a class="active" href="pagamentos.php">Pagamentos</a>
    <a href="planos.php">Planos</a>
    <a href="consultas.php">Consultas</a>
    <a href="exames.php">Exames</a>
</div>

<div>
    <form action="pagamentos.php" method="post" class="formulario">
        <table>
            <tr>
                <input type="hidden" id="id_pagamento" name="id_pagamento"
                       value="<?php echo $pagamento_edita->id_pagamento ?>">
                <label for="medico">Médico:</label>
                <select name="medico" id="medico">

                    <?php
                    foreach ($medicos as $medico): ?>
                        <option <?php echo preencherValores($pagamento_edita->fk_crm, $medico['crm']) ?>
                                value="<?php echo $medico['crm'] ?>"><?php echo $medico['nome_medico'] ?></option>
                    <?php endforeach ?>
                </select>
                <p>Valor Recebido Pelo Médico: <input type="number" step="0.01" name="recebimento_medico"
                                                      placeholder="R$..."
                                                      value="<?php echo $pagamento_edita->recebimento_medico ?>"/></p>
                <p>Comissão da Clínica: <input type="number" step="0.01" name="comissao_clinica" placeholder="R$..."
                                               value="<?php echo $pagamento_edita->comissao_clinica ?>"/></p>
                <p>Data de Recebimneto: <input type="date" name="dt_recebimento"
                                               value="<?php echo $pagamento_edita->dt_recebimento ?>"/></p>
                <p>Data de Repasse ao Médico: <input type="date" name="dt_repasse_medico"
                                                     value="<?php echo $pagamento_edita->dt_repasse_medico ?>"/></p>
            </tr>
        </table>
        <p><input type="submit" value="Salvar"/></p>
    </form>
</div>

<div>
    <div class="col-lg-4">
        <a href="pagamentos.php" target="_self">
            <button type="button" class="botao btn-primary btn-xl">
                <div>
                    <h3>Voltar</h3>
                </div>
            </button>
        </a>
    </div>
</div>
</body>

</html>