<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $endereco = new stdClass();
    $endereco->rua = $_POST['rua'];
    $endereco->numero = $_POST['numero'];
    $endereco->complemento = $_POST['complemento'];
    $endereco->bairro = $_POST['bairro'];
    $endereco->cidade = $_POST['cidade'];
    $endereco->estado = $_POST['estado'];
    if (!$POST_['id_endereco']) {
        $query_endereco = sprintf('CALL incluir_endereco("' . $endereco->rua . '","' . $endereco->numero . '","' . $endereco->complemento . '","' . $endereco->bairro . '","' . $endereco->cidade . '","' . $endereco->estado . '")');
        mysqli_query($conn, $query_endereco) or die(mysqli_error($conn));
        $query_id_endereco = "SELECT * FROM endereco ORDER BY id_endereco DESC LIMIT 0, 1";
        $result_endereco = mysqli_fetch_assoc(mysqli_query($conn, $query_id_endereco));
    }
    $funcionario_clinica = new stdClass();
    $funcionario_clinica->nome = $_POST['nome'];
    $funcionario_clinica->estado_civil = $_POST['estado_civil'];
    $funcionario_clinica->sexo = $_POST['sexo'];
    $funcionario_clinica->funcao = $_POST['funcao'];
    $funcionario_clinica->dt_nascimento = $_POST['dt_nascimento'];
    $funcionario_clinica->dt_admissao = $_POST['dt_admissao'];
    $funcionario_clinica->dependentes = $_POST['dependentes'];
    $funcionario_clinica->salario_bruto = $_POST['salario_bruto'];
    $funcionario_clinica->fk_id_plano = $_POST['plano_saude'];
    if ($_POST['id_funcionario']) {
        $funcionario_clinica->id_funcionario = $_POST['id_funcionario'];
        $query_endereco = sprintf('CALL editar_endereco("' . $endereco->id . '","' . $endereco->rua . '","' . $endereco->numero . '","' . $endereco->complemento . '","' . $endereco->bairro . '","' . $endereco->cidade . '","' . $endereco->estado . '")');
        mysqli_query($conn, $query_endereco) or die(mysqli_error($conn));
        $query = sprintf('CALL editar_funcionario("' . $funcionario_clinica->id_funcionario . '","' . $funcionario_clinica->nome . '","' . $funcionario_clinica->dt_nascimento . '","' . $funcionario_clinica->sexo . '","' . $result_endereco['id_endereco'] . '","' . $funcionario_clinica->funcao . '","' . $funcionario_clinica->dt_admissao . '","' . $funcionario_clinica->salario_bruto . '","' . $funcionario_clinica->estado_civil . '","' . $funcionario_clinica->dependentes . '","' . $funcionario_clinica->fk_id_plano . '")');
    } else {
        $query = sprintf('CALL incluir_funcionario("' . $funcionario_clinica->nome . '","' . $funcionario_clinica->dt_nascimento . '","' . $funcionario_clinica->sexo . '","' . $result_endereco['id_endereco'] . '","' . $funcionario_clinica->funcao . '","' . $funcionario_clinica->dt_admissao . '","' . $funcionario_clinica->salario_bruto . '","' . $funcionario_clinica->estado_civil . '","' . $funcionario_clinica->dependentes . '","' . $funcionario_clinica->fk_id_plano . '")');
    }
    mysqli_query($conn, $query) or die(mysqli_error($conn));
    header('http://bancodedados.freevar.com/funcionarios.php');
}

