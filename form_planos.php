<?php
require_once('config.php');
include('planosController.php');

if (isset($_GET['editar'])) {

    $query_plano = sprintf('SELECT * FROM plano_saude WHERE id_plano=' . $_GET['id'] . '');
    $plano_edita = mysqli_fetch_object(mysqli_query($conn, $query_plano)) or die(mysqli_error($conn));

}

?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Trabalho de Banco de Dados</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>

<body>
<div class="topnav">
    <a href="index.php">Home</a>
    <a href="clientes.php">Clientes</a>
    <a href="medicos.php">Médicos</a>
    <a href="funcionarios.php">Funcionários</a>
    <a href="pagamentos.php">Pagamentos</a>
    <a class="active" href="planos.php">Planos</a>
    <a href="consultas.php">Consultas</a>
    <a href="exames.php">Exames</a>
</div>

<div>
    <form action="planos.php" method="post" class="formulario">
        <table>
            <tr>
                <input type="hidden" id="id_plano" name="id_plano"
                       value="<?php echo $plano_edita->id_plano ?>">
                <p>Nome do Plano: <input type="text" name="nome" placeholder="Nome do plano..."
                                         value="<?php echo $plano_edita->nome ?>"/></p>
                <p>Registro Anvisa: <input type="text" name="registro_anvisa" placeholder="Registro Anvisa..."
                                           value="<?php echo $plano_edita->registro_anvisa ?>"/></p>
                <p>Valor da Consulta: <input type="number" step="0.01" name="valor_consulta" placeholder="Valor..."
                                             value="<?php echo $plano_edita->valor_consulta ?>"/></p>

        </table>
        <p><input type="submit" value="Salvar"/></p>
    </form>
</div>
<div>
    <div class="col-lg-4">
        <a href="planos.php" target="_self">
            <button type="button" class="botao btn-primary btn-xl">
                <div>
                    <h3>Voltar</h3>
                </div>
            </button>
        </a>
    </div>
</div>
</body>

</html>