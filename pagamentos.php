<?php
require_once('config.php');
include('pagamentosController.php');

$data = mysqli_query($conn, sprintf("SELECT * FROM info_pagamento ORDER BY dt_recebimento DESC")) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($data);
$result = mysqli_num_rows($data);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Trabalho de Banco de Dados</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
<div class="topnav">
    <a href="index.php">Home</a>
    <a href="clientes.php">Clientes</a>
    <a href="medicos.php">Médicos</a>
    <a href="funcionarios.php">Funcionários</a>
    <a class="active" href="pagamentos.php">Pagamentos</a>
    <a href="planos.php">Planos</a>
    <a href="consultas.php">Consultas</a>
    <a href="exames.php">Exames</a>
</div>
<div>
<?php if($result > 0) { ?>
    <table class="tabelas">
    <tr>
        <th>CRM</th>
        <th>Nome do Médico</th>
        <th>Valor Recebido Pelo Médico</th>
        <th>Comissão da Clínica</th>
        <th>Data de Recebimneto</th>
        <th>Data de Repasse ao Médico</th>
        <th>Imposto Retido</th>
        <th>Editar</th>
        <th>Deletar</th>
    </tr>

    <?
    do {
        ?>
        <tr>
            <td><?=$row['fk_crm'];?></td>
            <td><?= mysqli_fetch_object(mysqli_query($conn, sprintf("SELECT nome_medico FROM medico WHERE crm=".$row['fk_crm'])))->nome_medico;?></td>
            <td>R$: <?=number_format($row['recebimento_medico'], 2, ',','.');?></td>
            <td>R$: <?=number_format($row['comissao_clinica'], 2, ',','.');?></td>
            <td><?=date("d/m/Y", strtotime($row['dt_recebimento']));?></td>
            <td><?=date("d/m/Y", strtotime($row['dt_repasse_medico']));?></td>
            <td>R$: <?=number_format(($row['imposto_retido']), 2, ',','.');?></td>
            <td>
                <center>
                    <form action="form_pagamento.php" method="get" target="_top">
                        <input type="hidden" id="editar" name="editar">
                        <input type="hidden" id="id" name="id" value="<?=$row['id_pagamento'];?>">
                        <button type="submit" class="btn-danger" id="editar">E</button>
                    </form>
                </center>
            </td>
            <td>
                <center>
                    <form action="pagamentos.php" method="get" target="_self">
                        <input type="hidden" id="deletar" name="deletar">
                        <input type="hidden" id="id" name="id" value="<?=$row['id_pagamento'];?>">
                        <button type="submit" class="btn-danger" id="deletar">X</button>
                    </form>
                </center>
            </td>
        </tr>
    <?
    }while($row = mysqli_fetch_assoc($data));

    ?></table><?
}
?>
    <div class="col-lg-4">
        <a href="form_pagamento.php" target="_top">
            <button type="button" class="botao btn-primary btn-xl">Cadastrar Pagamento</button>
        </a>
    </div>


</div>
</body>
</html>
<?php
if (isset($_GET['deletar']))
{
    deletar($_GET['id'], $conn);
}

function deletar($id, $conn)
{
    $query = sprintf('CALL excluir_pagamento('.$id.')');
    mysqli_query($conn, $query) or die(mysqli_error($conn));
    header('Location: http://bancodedados.freevar.com/pagamentos.php');
}

mysqli_free_result($data);
?>