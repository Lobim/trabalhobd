<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $consulta = new stdClass();
    $consulta->nome_consulta = $_POST['nome'];
    $consulta->fk_id_medico = $_POST['medico'];
    $consulta->fk_id_cliente = $_POST['cliente'];
    $consulta->fk_id_exame = $_POST['exame'];
    $consulta->fk_id_pagamento = $_POST['pagamento'];
    $consulta->dt_recebimento = mysqli_fetch_assoc(mysqli_query($conn, 'select dt_recebimento from info_pagamento where id_pagamento = '.$consulta->fk_id_pagamento.'')) or die(mysqli_error($conn));

    if ($_POST['id_consulta']) {
        $consulta->id = $_POST['id_consulta'];
        $query = sprintf('CALL editar_consulta("' . $consulta->id . '","' . $consulta->nome_consulta . '","' . $consulta->fk_id_cliente . '","' . $consulta->fk_id_exame . '","' . $consulta->fk_id_medico . '","' . $consulta->fk_id_pagamento . '")');
    } else {
        $query = sprintf('CALL incluir_consulta("' . $consulta->nome_consulta . '","' . $consulta->fk_id_cliente . '","' . $consulta->fk_id_exame . '","' . $consulta->fk_id_medico . '","' . $consulta->fk_id_pagamento . '")');
    }
    mysqli_query($conn, $query) or die(mysqli_error($conn));

    $query_cliente_data = sprintf('UPDATE cliente SET data_ultima_consulta = "'.$consulta->dt_recebimento['dt_recebimento'].'" WHERE id_cliente = "'.$consulta->fk_id_cliente.'"');

    mysqli_query($conn, $query_cliente_data) or die(mysqli_error($conn));

    header('http://bancodedados.freevar.com/consultas.php');
}
