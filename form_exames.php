<?php
require_once('config.php');
include('examesController.php');

$query_exames = sprintf("SELECT * FROM exames");
$data_exames = mysqli_query($conn, $query_exames) or die(mysqli_error($conn));

if (isset($_GET['editar'])) {

    $query_consultas = sprintf('SELECT * FROM exames WHERE id_exame=' . $_GET['id'] . '');
    $exame_edita = mysqli_fetch_object(mysqli_query($conn, $query_consultas)) or die(mysqli_error($conn));

}

?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Trabalho de Banco de Dados</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>

<body>
<div class="topnav">
    <a href="index.php">Home</a>
    <a href="clientes.php">Clientes</a>
    <a href="medicos.php">Médicos</a>
    <a href="funcionarios.php">Funcionários</a>
    <a href="pagamentos.php">Pagamentos</a>
    <a href="planos.php">Planos</a>
    <a href="consultas.php">Consultas</a>
    <a class="active" href="exames.php">Exames</a>
</div>

<div>
    <form action="exames.php" method="post" class="formulario">
        <table>
            <tr>
                <input type="hidden" id="id_exame" name="id_exame" value="<?php echo $exame_edita->id_exame ?>">
                <p>Nome: <input type="text" name="nome" placeholder="Nome do Exame..."
                                value="<?php echo $exame_edita->nome_exame ?>"/></p>
            </tr>
        </table>
        <p><input type="submit" value="Salvar"/></p>
    </form>
</div>
<div>
    <div class="col-lg-4">
        <a href="exames.php" target="_self">
            <button type="button" class="botao btn-primary btn-xl">
                <div>
                    <h3>Voltar</h3>
                </div>
            </button>
        </a>
    </div>
</div>
</body>

</html>