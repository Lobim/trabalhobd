<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $endereco = new stdClass();
    $endereco->rua = $_POST['rua'];
    $endereco->numero = $_POST['numero'];
    $endereco->complemento = $_POST['complemento'];
    $endereco->bairro = $_POST['bairro'];
    $endereco->cidade = $_POST['cidade'];
    $endereco->estado = $_POST['estado'];
    if (!$POST_['id_endereco']) {
        $query_endereco = sprintf('CALL incluir_endereco("' . $endereco->rua . '","' . $endereco->numero . '","' . $endereco->complemento . '","' . $endereco->bairro . '","' . $endereco->cidade . '","' . $endereco->estado . '")');
        mysqli_query($conn, $query_endereco) or die(mysqli_error($conn));
        $query_id_endereco = "SELECT * FROM endereco ORDER BY id_endereco DESC LIMIT 0, 1";
        $result_endereco = mysqli_fetch_assoc(mysqli_query($conn, $query_id_endereco));
    }
    $cliente = new stdClass();
    $cliente->nome = $_POST['nome'];
    $cliente->estado_civil = $_POST['estado_civil'];
    $cliente->sexo = $_POST['sexo'];
    $cliente->doenca = $_POST['doenca'];
    $cliente->doenca_cronica = isset($_POST['doenca_cronica']);
    $cliente->idade = $_POST['idade'];
    $cliente->plano_saude = $_POST['plano_saude'];
    if ($_POST['id_cliente']) {
        $cliente->id = $_POST['id_cliente'];
        $query_endereco = sprintf('CALL editar_endereco("' . $endereco->id . '","' . $endereco->rua . '","' . $endereco->numero . '","' . $endereco->complemento . '","' . $endereco->bairro . '","' . $endereco->cidade . '","' . $endereco->estado . '")');
        mysqli_query($conn, $query_endereco) or die(mysqli_error($conn));
        $query = sprintf('CALL editar_cliente("' . $cliente->id . '","' . $cliente->nome . '","' . $cliente->estado_civil . '","' . $cliente->sexo . '","' . $cliente->doenca . '","' . $cliente->doenca_cronica . '","' . $cliente->idade . '","' . $result_endereco['id_endereco'] . '","' . $cliente->plano_saude . '")');
    } else {
        $query = sprintf('CALL incluir_cliente("' . $cliente->nome . '","' . $cliente->estado_civil . '","' . $cliente->sexo . '","' . $cliente->doenca . '","' . $cliente->doenca_cronica . '","' . $cliente->idade . '","' . $result_endereco['id_endereco'] . '","' . $cliente->plano_saude . '")');
    }
    mysqli_query($conn, $query) or die(mysqli_error($conn));
    header('http://bancodedados.freevar.com/clientes.php');
}
