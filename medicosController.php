<?php

if ($_SERVER["REQUEST_METHOD"] == "POST" ) {
    // collect value of input field
    $medico = new stdClass();
    $medico->crm = $_POST['crm'];
    $medico->nome = $_POST['nome_medico'];
    $medico->dt_nascimento = $_POST['dt_nascimento'];
    $medico->estado_civil = $_POST['estado_civil'];
    $medico->escola_origem = $_POST['escola_origem'];
    $medico->tipo_resid_medica = $_POST['tipo_resid_medica'];
    $medico->especialidade = $_POST['especialidade'];
    $medico->plano_atende = $_POST['plano_saude'];
    $medico->regime_trabalho = $_POST['regime_trabalho'];

    if(mysqli_num_rows(mysqli_query($conn, sprintf('SELECT * FROM medico WHERE crm = '.$medico->crm)))>0){
        $query_medico = sprintf('CALL editar_medico("'.$medico->crm.'","'.$medico->nome.'","'.$medico->especialidade.'","'.$medico->escola_origem.'","'.$medico->tipo_resid_medica.'","'.$medico->estado_civil.'","'.$medico->regime_trabalho.'","'.$medico->plano_atende.'","'.$medico->dt_nascimento.'")');
    }else{
        $query_medico = sprintf('CALL incluir_medico("'.$medico->crm.'","'.$medico->nome.'","'.$medico->especialidade.'","'.$medico->escola_origem.'","'.$medico->tipo_resid_medica.'","'.$medico->estado_civil.'","'.$medico->regime_trabalho.'","'.$medico->plano_atende.'","'.$medico->dt_nascimento.'")');
    }

    mysqli_query($conn, $query_medico) or die(mysqli_error($conn));

    header('http://bancodedados.freevar.com/medicos.php');
}