<?php
require_once('config.php');
include('funcionariosController.php');

$query = sprintf("SELECT * FROM plano_saude");
$data = mysqli_query($conn, $query) or die(mysqli_error($conn));

while ($row = mysqli_fetch_assoc($data)) {
    $planos[] = $row;
}

$result = mysqli_num_rows($data);

if (isset($_GET['editar'])) {

    $query_funcionario = sprintf('SELECT * FROM funcionario_clinica WHERE id_funcionario=' . $_GET['id'] . '');
    $funcionario_edita = mysqli_fetch_object(mysqli_query($conn, $query_funcionario)) or die(mysqli_error($conn));

    $result_endereco = mysqli_fetch_object(mysqli_query($conn, 'SELECT * FROM endereco WHERE id_endereco =' . $funcionario_edita->fk_id_endereco));

}

function preencherValores($valor, $item)
{
    if (isset($valor) && $valor == $item) {
        return "selected";
    }
    return "";
}

?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Trabalho de Banco de Dados</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>

<body>
<div class="topnav">
    <a href="index.php">Home</a>
    <a href="clientes.php">Clientes</a>
    <a href="medicos.php">Médicos</a>
    <a class="active" href="funcionarios.php">Funcionários</a>
    <a href="pagamentos.php">Pagamentos</a>
    <a href="planos.php">Planos</a>
    <a href="consultas.php">Consultas</a>
    <a href="exames.php">Exames</a>
</div>

<div>
    <form action="funcionarios.php" method="post" class="formulario">
        <table>
            <tr>
                <input type="hidden" id="id_endereco" name="id_endereco"
                       value="<?php echo $funcionario_edita->fk_id_endereco ?>">
                <input type="hidden" id="id_cliente" name="id_funcionario"
                       value="<?php echo $funcionario_edita->id_funcionario ?>">
                <p>Nome: <input type="text" name="nome" placeholder="Nome do funcionario..."
                                value="<?php echo $funcionario_edita->nome ?>"/></p>
                <p>Estado Civil: <input type="text" name="estado_civil" placeholder="Estado civil..."
                                        value="<?php echo $funcionario_edita->estado_civil ?>"/></p>
                <p>sexo: <input type="text" name="sexo" placeholder="Sexo..."
                                value="<?php echo $funcionario_edita->sexo ?>"/></p>
                <p>Funcao: <input type="text" name="funcao" placeholder="Função..."
                                  value="<?php echo $funcionario_edita->funcao ?>"/></p>
                <p>Data de Nascimento: <input type="date" name="dt_nascimento"
                                              value="<?php echo $funcionario_edita->dt_nascimento ?>"/></p>
                <p>Data de Admissão: <input type="date" name="dt_admissao"
                                            value="<?php echo $funcionario_edita->dt_admissao ?>"/></p>
                <p>Dependentes: <input type="text" name="dependentes" placeholder="Dependentes..."
                                       value="<?php echo $funcionario_edita->dependentes ?>"/></p>
                <p>Salário: <input type="text" name="salario_bruto" placeholder="Salário..."
                                   value="<?php echo $funcionario_edita->salario_bruto ?>"/></p>
                <label for="plano_saude">Plano de saúde:</label>
                <select name="plano_saude" id="plano_saude">
                    <?php foreach ($planos as $plano_saude): ?>
                        <option <?php echo preencherValores($funcionario_edita->fk_id_plano, $plano_saude['id_plano']) ?>
                                value="<?php echo $plano_saude['id_plano'] ?>"><?php echo $plano_saude['nome'] ?></option>
                    <?php endforeach ?>
                </select>
            </tr>
            <tr>

                <p>Rua: <input type="text" name="rua" placeholder="Rua..." value="<?php echo $result_endereco->rua ?>"/>
                </p>

                <p>Número: <input type="text" name="numero" placeholder="Número..."
                                  value="<?php echo $result_endereco->numero ?>"/></p>

                <p>Complemento: <input type="text" name="complemento" placeholder="Ex: apto, bloco..."
                                       value="<?php echo $result_endereco->complemento ?>"/></p>

                <p>Bairro: <input type="text" name="bairro" placeholder="Bairro..."
                                  value="<?php echo $result_endereco->bairro ?>"/></p>

                <p>Cidade: <input type="text" name="cidade" placeholder="Cidade..."
                                  value="<?php echo $result_endereco->cidade ?>"/></p>

                <p>Estado: <input type="text" name="estado" placeholder="Estado..."
                                  value="<?php echo $result_endereco->estado ?>"/></p>

            </tr>
        </table>
        <p><input type="submit" value="Salvar"/></p>
    </form>
</div>
<div>
    <div class="col-lg-4">
        <a href="funcionarios.php" target="_self">
            <button type="button" class="botao btn-primary btn-xl">
                <div>
                    <h3>Voltar</h3>
                </div>
            </button>
        </a>
    </div>
</div>
</body>
</html>