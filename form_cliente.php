<?php
require_once('config.php');
include('clientesController.php');

$query = sprintf("SELECT * FROM plano_saude");
$data = mysqli_query($conn, $query) or die(mysqli_error($conn));

while ($row = mysqli_fetch_assoc($data)) {
    $planos[] = $row;
}

if (isset($_GET['editar'])) {

    $query_cliente = sprintf('SELECT * FROM cliente WHERE id_cliente=' . $_GET['id'] . '');
    $cliente_edita = mysqli_fetch_object(mysqli_query($conn, $query_cliente)) or die(mysqli_error($conn));
    $result_endereco = mysqli_fetch_object(mysqli_query($conn, 'SELECT * FROM endereco WHERE id_endereco =' . $cliente_edita->fk_id_endereco));

}

function preencherValores($valor, $item)
{
    if (isset($valor) && $valor == $item) {
        return "selected";
    }
    return "";
}

function checaDoencaCronica($valor){
    if (isset($valor) && $valor == 1) {
        return "checked";
    }
    return "";
}

?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Trabalho de Banco de Dados</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>

<body>
<div class="topnav">
    <a href="index.php">Home</a>
    <a class="active" href="clientes.php">Clientes</a>
    <a href="medicos.php">Médicos</a>
    <a href="funcionarios.php">Funcionários</a>
    <a href="pagamentos.php">Pagamentos</a>
    <a href="planos.php">Planos</a>
    <a href="consultas.php">Consultas</a>
    <a href="exames.php">Exames</a>
</div>

<div>
    <form action="clientes.php" method="post" class="formulario">
        <table>
            <tr>
                <input type="hidden" id="id_endereco" name="id_endereco"
                       value="<?php echo $cliente_edita->fk_id_endereco ?>">
                <input type="hidden" id="id_cliente" name="id_cliente" value="<?php echo $cliente_edita->id_cliente ?>">
                <p>Nome: <input type="text" name="nome" placeholder="Nome do cliente..."
                                value="<?php echo $cliente_edita->nome ?>"/></p>
                <p>Estado Civil: <input type="text" name="estado_civil" placeholder="Estado civil..."
                                        value="<?php echo $cliente_edita->estado_civil ?>"/></p>
                <p>sexo: <input type="text" name="sexo" placeholder="Sexo..."
                                value="<?php echo $cliente_edita->sexo ?>"/></p>
                <p>Doença Pré Existente: <input type="text" name="doenca" placeholder="Doença pré-existente..."
                                                value="<?php echo $cliente_edita->doenca_pre_existente ?>"/></p>
                <p>Doença Crônica: <input type="checkbox" id="doenca_cronica" name="doenca_cronica" value="<?php echo $cliente_edita->doenca_cronica ?>" <?php echo checaDoencaCronica($cliente_edita->doenca_cronica)?>></p>

                <p>Idade: <input type="text" name="idade" value="<?php echo $cliente_edita->idade ?>"/></p>

                <label for="plano_saude">Plano de saúde:</label>

                <select name="plano_saude" id="plano_saude">

                    <?php foreach ($planos as $plano_saude): ?>
                        <option <?php echo preencherValores($cliente_edita->fk_id_plano, $plano_saude['id_plano']) ?>
                                value="<?php echo $plano_saude['id_plano'] ?>"><?php echo $plano_saude['nome'] ?></option>
                    <?php endforeach ?>
                </select>
            </tr>
            <tr>

                <p>Rua: <input type="text" name="rua" placeholder="Rua..." value="<?php echo $result_endereco->rua ?>"/>
                </p>

                <p>Número: <input type="text" name="numero" placeholder="Número..."
                                  value="<?php echo $result_endereco->numero ?>"/></p>

                <p>Complemento: <input type="text" name="complemento" placeholder="Ex: apto, bloco..."
                                       value="<?php echo $result_endereco->complemento ?>"/></p>

                <p>Bairro: <input type="text" name="bairro" placeholder="Bairro..."
                                  value="<?php echo $result_endereco->bairro ?>"/></p>

                <p>Cidade: <input type="text" name="cidade" placeholder="Cidade..."
                                  value="<?php echo $result_endereco->cidade ?>"/></p>

                <p>Estado: <input type="text" name="estado" placeholder="Estado..."
                                  value="<?php echo $result_endereco->estado ?>"/></p>

            </tr>
        </table>
        <p><input type="submit" value="Salvar"/></p>
    </form>
</div>
<div>
    <div class="col-lg-4">
        <a href="clientes.php" target="_self">
            <button type="button" class="botao btn-primary btn-xl">
                <div>
                    <h3>Voltar</h3>
                </div>
            </button>
        </a>
    </div>
</div>
</body>

</html>