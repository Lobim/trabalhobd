<?php
require_once('config.php');
include('funcionariosController.php');

$data = mysqli_query($conn, sprintf("SELECT * FROM listar_funcionarios")) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($data);
$result = mysqli_num_rows($data);


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Trabalho de Banco de Dados</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
<div class="topnav">
    <a href="index.php">Home</a>
    <a href="clientes.php">Clientes</a>
    <a href="medicos.php">Médicos</a>
    <a class="active" href="funcionarios.php">Funcionários</a>
    <a href="pagamentos.php">Pagamentos</a>
    <a href="planos.php">Planos</a>
    <a href="consultas.php">Consultas</a>
    <a href="exames.php">Exames</a>
</div>
<div>
<?php
// se o número de resultados for maior que zero, mostra os dados
if($result > 0) {
    // inicia o loop que vai mostrar todos os dados ?>
    <table class="tabelas">
    <tr>
        <th>Nome</th>
        <th>Estado Civil</th>
        <th>Sexo</th>
        <th>Funcao</th>
        <th>Data de Nascimento</th>
        <th>Data de Admissão</th>
        <th>Dependentes</th>
        <th>Salario Bruto</th>
        <th>Plano de Saúde</th>
        <th colspan="6">Endereço </th>
        <th>Editar</th>
        <th>Deletar</th>
    </tr>

    <?
    do {
        ?>
        <tr>
            <td><?=$row['nome'];?></td>
            <td><?=$row['estado_civil'];?></td>
            <td><?=$row['sexo'];?></td>
            <td><?=$row['funcao'];?></td>
            <td><?=date("d/m/Y", strtotime($row['dt_nascimento']));?></td>
            <td><?=date("d/m/Y", strtotime($row['dt_admissao']));?></td>
            <td><?=$row['dependentes'];?></td>
            <td><?=$row['salario_bruto'];?></td>
            <td><?= mysqli_fetch_object(mysqli_query($conn, sprintf("SELECT nome FROM listar_plano WHERE id_plano=".$row['fk_id_plano'])))->nome;?></td>
            <td>Rua: <?=$row['rua'];?></td>
            <td>Número: <?=$row['numero'];?></td>
            <td>Complemento: <?=$row['complemento'];?></td>
            <td>Bairro: <?=$row['bairro'];?></td>
            <td>Cidade: <?=$row['cidade'];?></td>
            <td>Estado: <?=$row['estado'];?></td>
            <td>
                <center>
                    <form action="form_funcionarios.php" method="get" target="_top">
                        <input type="hidden" id="editar" name="editar">
                        <input type="hidden" id="id" name="id" value="<?=$row['id_funcionario'];?>">
                        <button type="submit" class="btn-danger" id="editar">E</button>
                    </form>
                </center>
            </td>
            <td>
                <center>
                    <form action="funcionarios.php" method="get" target="_self">
                        <input type="hidden" id="deletar" name="deletar">
                        <input type="hidden" id="id" name="id" value="<?=$row['id_funcionario'];?>">
                        <button type="submit" class="btn-danger" id="deletar">X</button>
                    </form>
                </center>
            </td>
        </tr>
    <?
    }while($row = mysqli_fetch_assoc($data));

    ?></table><?
}
?>
    <div class="col-lg-4">
        <a href="form_funcionarios.php" target="_top">
            <button type="button" class="botao btn-primary btn-xl">Cadastrar Funcionário</button>
        </a>
    </div>


</div>
</body>
</html>
<?php
//check if the get variable exists
if (isset($_GET['deletar']))
{
    deletar($_GET['id'], $conn);
}

function deletar($id, $conn)
{
    $query = sprintf('CALL excluir_funcionario('.$id.')');
    mysqli_query($conn, $query) or die(mysqli_error($conn));
    header('Location: http://bancodedados.freevar.com/funcionarios.php');
}
//check if the get variable exists

mysqli_free_result($data);
?>