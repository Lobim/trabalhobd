<?php
require_once('config.php');
include('clientesController.php');

$query_medicos = sprintf("SELECT * FROM medico");
$query_clientes = sprintf("SELECT * FROM cliente");
$query_exames = sprintf("SELECT * FROM exames");
$query_pagamentos = sprintf("SELECT * FROM info_pagamento");
$data_medicos = mysqli_query($conn, $query_medicos) or die(mysqli_error($conn));
$data_clientes = mysqli_query($conn, $query_clientes) or die(mysqli_error($conn));
$data_exames = mysqli_query($conn, $query_exames) or die(mysqli_error($conn));
$data_pagamentos = mysqli_query($conn, $query_pagamentos) or die(mysqli_error($conn));

while ($row_medicos = mysqli_fetch_assoc($data_medicos)) {
    $medicos[] = $row_medicos;
}
while ($row_clientes = mysqli_fetch_assoc($data_clientes)) {
    $clientes[] = $row_clientes;
}
while ($row_exames = mysqli_fetch_assoc($data_exames)) {
    $exames[] = $row_exames;
}
while ($row_pagamentos = mysqli_fetch_assoc($data_pagamentos)) {
    $pagamentos[] = $row_pagamentos;
}




if (isset($_GET['editar'])) {

    $query_consultas = sprintf('SELECT * FROM consultas WHERE id_consultas=' . $_GET['id'] . '');
    $consulta_edita = mysqli_fetch_object(mysqli_query($conn, $query_consultas)) or die(mysqli_error($conn));

}

function preencherValores($valor, $item)
{
    if (isset($valor) && $valor == $item) {
        return "selected";
    }
    return "";
}


?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Trabalho de Banco de Dados</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>

<body>
<div class="topnav">
    <a href="index.php">Home</a>
    <a href="clientes.php">Clientes</a>
    <a href="medicos.php">Médicos</a>
    <a href="funcionarios.php">Funcionários</a>
    <a href="pagamentos.php">Pagamentos</a>
    <a href="planos.php">Planos</a>
    <a class="active" href="consultas.php">Consultas</a>
    <a href="exames.php">Exames</a>
</div>

<div>
    <form action="consultas.php" method="post" class="formulario">
        <table>
            <tr>
                <input type="hidden" id="id_consulta" name="id_consulta" value="<?php echo $consulta_edita->id_consultas ?>">
                <p>Nome: <input type="text" name="nome" placeholder="Nome da Consulta..."
                                value="<?php echo $consulta_edita->nome_consulta ?>"/></p>
                <label for="medico">Médico:</label>
                <select name="medico" id="medico">
                    <?php foreach ($medicos as $medico): ?>
                        <option <?php echo preencherValores($consulta_edita->fk_crm, $medico['crm']) ?>
                                value="<?php echo $medico['crm'] ?>"><?php echo $medico['nome_medico'] ?></option>
                    <?php endforeach ?>
                </select>
                <label for="cliente">Cliente:</label>
                <select name="cliente" id="cliente">
                    <?php foreach ($clientes as $cliente): ?>
                        <option <?php echo preencherValores($consulta_edita->fk_id_cliente, $cliente['id_cliente']) ?>
                                value="<?php echo $cliente['id_cliente'] ?>"><?php echo $cliente['nome'] ?></option>
                    <?php endforeach ?>
                </select>
                <label for="exame">Exame:</label>
                <select name="exame" id="exame">
                    <?php foreach ($exames as $exame): ?>
                        <option <?php echo preencherValores($consulta_edita->fk_id_exame, $exame['id_exame']) ?>
                                value="<?php echo $exame['id_exame'] ?>"><?php echo $exame['nome_exame'] ?></option>
                    <?php endforeach ?>
                </select>
                <label for="pagamento">Data Pagamento:</label>
                <select name="pagamento" id="pagamento">
                    <?php foreach ($pagamentos as $pagamento): ?>
                        <option <?php echo preencherValores($consulta_edita->fk_id_exame, $pagamento['id_pagamento']) ?>
                                value="<?php echo $pagamento['id_pagamento'] ?>"><?php echo $pagamento['dt_recebimento'] ?></option>
                    <?php endforeach ?>
                </select>
            </tr>
        </table>
        <p><input type="submit" value="Salvar"/></p>
    </form>
</div>
<div>
    <div class="col-lg-4">
        <a href="consultas.php" target="_self">
            <button type="button" class="botao btn-primary btn-xl">
                <div>
                    <h3>Voltar</h3>
                </div>
            </button>
        </a>
    </div>
</div>
</body>

</html>